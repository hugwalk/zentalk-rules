# 如何編譯 TWRP touch recovery？
翻譯進度：5% | 作者：pan93412

## 概述
所有的 TWRP 3.x 原始碼皆是開源的，因此你可以自己編譯。這教學不打算以 step-by-step（步驟式引導）、或是 word-for-word（逐字引導）的類型教學。所以如果你不熟悉基本 Linux 指令或 AOSP 中編譯，那你就不應該編譯 TWRP。<del>滾回去洗洗睡啦</del>

> Translator said: 英文也要有一定的水準，假如你熟悉 Linux 指令、也知道怎麼於 AOSP 編譯，但你卻看不懂開發的相關專有名詞，還是建議你先去看中文的引導式教學吧。:)

你目前可以使用 Omni 5.1、Omni 6.0、Omni 7.1、Omni 8.1、CM 12.1、CM 13.0、CM 14.1 或 CM 15.1 的原始碼，而我們比較推薦 Omni 7.1。你可以於 CM 編譯，但你可能會遇到一點點點的小問題。如果你不知道怎麼解決一些小問題，那還是勸你使用 Omni 吧， <del>別把時間花在 Debug 上 XD</del>

假如你正在使用 CM，你將需要在 CM/bootable/recovery-twrp 資料夾中放入 TWRP，並在你的 BoardConfig.mk 檔案中設定 `RECOVERY_VARIANT := twrp`。TWRP 的原始碼可以在 [這裡](https://github.com/omnirom/android_bootable_recovery) 找到。<br />
選擇可以使用的 **最新分支** ，這步驟對 Omni 來說是多餘的，因為 Omni 已經預先包含了 TWRP 來源，但是！當你使用較舊版本的 Omni，你可能會想要從最新分支抓取最新的 TWRP 來源。（最新的分支將可以在舊的編譯樹上順利編譯歐！）

#
